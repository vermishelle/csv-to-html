require 'csv'
require 'cgi' # to html-escape

unless ARGV[0]
  puts "usage: ruby csv2html.rb yourfile.csv"
  puts "It will output result to output.html"
end

options = {col_sep: ';', headers: true}
csv_table = CSV.read(ARGV[0], options)

html = ""

html << "<table>"

html << "<thead>"
csv_table.headers.each do |header|
  html << "<td>"
  html << CGI.escapeHTML(header)
  html << "</td>"
end
html << "</thead>"

html << "<tbody>"
csv_table.each do |row|
  html << "<tr>"
  row.each do |cell|
    html << "<td>"

    html << CGI.escapeHTML(cell[1].to_s)
    html << "</td>"
  end
  html << "</tr>"
end
html << "</tbody>"

html << "</table>"

File.write('output.html', html)
